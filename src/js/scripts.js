const header = document.querySelector(".header");
const menu = document.querySelector(".mobile-version");
const menuName =document.querySelector(".content__list");
const itemMenu = menu.children;
const itemName = menuName.children;
menu.addEventListener('click', openMenu);

function openMenu () {
    for( elem of itemMenu){
        elem.classList.toggle('active');
    }
    if(itemMenu[1].classList.value === "mobile-version__close active"){
        const ul =document.createElement('ul');
        ul.classList.add('burger-menu');
        header.append(ul);
        for (const name of itemName) {
            const li =document.createElement('li');
            li.classList.add('burger-menu__item');
            ul.append(li);
            const p =document.createElement('p');
            p.classList.add('burger-menu__item-text');
            p.innerText = name.innerText;
            li.append(p);
        }
    } 
    if(itemMenu[0].classList.value === "mobile-version__menu active"){
        const objectUl = document.querySelector(".burger-menu");
        objectUl.remove();
    } 
}

window.addEventListener('resize', () =>{
    if(document.body.clientWidth > 768){
        const objectUl = document.querySelector(".burger-menu");
        objectUl.remove();
        if(itemMenu[1].classList.value === "mobile-version__close active"){
            for( elem of itemMenu){
                elem.classList.toggle('active');
            }
        }    
    }
})

