import * as nodePath from 'path';
const rootFolder = nodePath.basename(nodePath.resolve());



const buildFolder =`./dist`;
const srcFolder =`./src`;

export const path = {
    build:{
        images:`${buildFolder}/img/`,
        js:`${buildFolder}/js/`,
        css:`${buildFolder}/css`,
        html:`${buildFolder}/`,
    },
    src:{
        js:`${srcFolder}/js/scripts.js`,
        images:`${srcFolder}/img/**/*.{jpg,jpeg,png,gif,webp}`,
        svg:`${srcFolder}/img/**/*.svg`,
        scss:`${srcFolder}/scss/style.scss`,
        html:`${srcFolder}/*.html`,
    },
    watch:{
        images:`${srcFolder}/img/**/*.{jpg,jpeg,png,gif,webp,svg,ico}`,
        js:`${srcFolder}/js/**/*.js`,
        scss:`${srcFolder}/scss/**/*.scss`,
        html:`${srcFolder}/**/*.html`,
    },
    clean: buildFolder,
    buildFolder: buildFolder,
    srcFolder:srcFolder,
    rootFolder:rootFolder,
}